from random import randint
#Asks for human's name
user_name = input("Hi! What is your name? ")
for guess_number in range(5):
    birthmonth = randint(1, 12)
    birthyear = randint(1924, 2004)
    guess = guess_number+1
    print("Guess number " + str(guess))
    print(user_name, "were you born in", birthmonth, "/", birthyear, "?")
    guess_response = input("")
    if guess_response == "no" and guess <5:
        print("Drat! Lemme try again")
    elif guess_response == "yes":
        print("I knew it!")
        break
    else:
        print("I have other things to do.  Good bye.")

